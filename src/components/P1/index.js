import React from'react';
import "./style.css";
import a2 from "../../assets/a2.jpg"
import k3 from "../../assets/k3.jpg"


const P1 = props =>{
    return(
        
   
    <div className="p1">
        
        <img className="im" src={a2} alt =" Post Image"></img>
        <div className="des" >
            <div className="im2">Caractéristiques
            <p><a className="pa1" >Date:</a> <a className="pa2">Février 2020</a></p>
            <p><a className="pa1">Type:</a> <a  className="pa2">Site vitrine</a></p>
            <p><a className="pa1">Secteur:</a> <a className="pa2">Droit / Avocats</a></p>
            <p><a className="pa1">Technologies:</a> <a className="pa2">HTML 5, CSS 3</a></p>
            </div>
            <div className="im2">Description
            <p className="pa2">Il y a 3 ans, En collaboration avec l'agence Eliott & Markus, nous avons signé la première version du site du célèbre cabinet d'avocats August Debouzy.</p>
            </div>
             
            
            </div>
            <div className="des1">
                <p>En février dernier, le cabinet a renouvelé sa confiance et nous a donné comme mission d'intégrer le nouveau web-design </p>
            <p>réalisé par Eliott & Markus. De plus, nous enrichissons régulièrement le site avec la rubrique 'Grands Angles' dans laquelle,</p>
            <p>chaque page de contenu jouit d'une intégration très exigeante.</p>
            <div>
            <p>Nous sommes particulièrement fiers de suivre dans la durée nos clients car c'est dans cette démarche de développement </p>
            <p>continu que nous pouvons exprimer pleinement nos compétences d'artisans du web.</p>
            </div>
            <img className="imo" src={k3} alt =" Post Image"></img>
            </div>
            </div>
          
   
    
    );
}

export default P1;